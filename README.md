# seapoint_turbidity_driver

Crappy first-rev ROS driver for interfacing to the [Seapoint turbidity interface](https://gitlab.com/amarburg/turbidity_sensor_board_firmware).

**Current Status:**   Message publication and service call to change unit range work.  Auto-ranging has been implemented but not tested.

# Messages

Defines two message types:

 * [WaterTurbidity](msg/WaterTurbidity.msg) is a simple vendor-agnostic message which reports only NTUs.
 * [SeapointTurbidity](msg/SeapointTurbidity.msg) includes raw data types specific to the Seapoint turbidity interface.

 # Services

 Defines one service:

 * [SeapointRange](srv/SeapointRange.srv) is used to change the range (gain / sensitivity) of the Seapoint device.  It can also be set to `SEAPOINT_AUTO` which places the sensor in an autoranging mode.

 # License

 This node is released under the [BSD 3-Clause License.](LICENSE)
